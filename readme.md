# Ứng dụng "Lib Chart" và "Alias" trong việc quản lý ứng dụng được triển khai dưới mô hình microservice 

# 1. Vấn đề quản lý Chart cho các ứng dụng 
Bài toán đặt ra là bạn có nhiều ứng dụng triển khai trên Kubernetes dưới dưới các dịch vụ được kết nối với nhau.

Vấn đề đặt ra là mỗi khi có sự thay đổi (một vài dịch vụ hoặc tất cả). Giả sử một số ứng dụng cần được mở NodePort hoặc cần mount log ra ngoài Worker. Khi đó, bạn phải vào từng manifest của từng dịch vụ để thực hiện các thao tác lặp đi lặp lại (Ctrl + C, Ctrl + V) từ manifest của dịnh vụ này qua dịch vụ khác... Công việc này rất nhàn chán và mất thời gian. 

*Vậy có cách nào hỗ trợ bạn trong việc quản lý các file manifest cho các ứng dụng một các hiệu quả?*

=> "Lib Chart" trong Helm sẽ giúp bạn xử lý việc này một các hiệu quả.

# 2. Đối tượng trình bày
Trong bài viết này mình sẽ trình bày về cách mình ứng dụng "Lib Chart" và "Alias" trong việc triển khai dự án.

Nếu được thiết kế tốt toàn bộ các dịch vụ của bạn sẽ được triển khai chỉ cần một "Lib Chart" cùng với "Alias".

- Lib Chart: được hiểu tương tự như các dependencies của Java. Khi bạn cần sử dụng thư viện nào bạn chỉ cần cấu hình thêm thư viện trong file "pom.xml" và thực hiện **mvn install** ứng dụng của bạn sẽ có thư viện của bạn cần dùng. Và việc cấu hình Lib Chart cho Chart trong helm cũng thực hiện tương tự. Mình sẽ giới thiệu cụ thể ở phần 3.
- Alias: Sử dung alias khi bạn muốn tái sử dụng lại những Chart trong một Chart cụ thể (Ví dụ: Để triển khai dịch vụ A, bạn đã viết ra Helm Chart tên là helm-a. Dịch vụ A cần triển khai 2 helm-a với 2 cấu hình khác nhau. Khi đó ta sẽ cần sử dụng **alias** vừa đảm bảo nghiệp vụ mà không cần quản duplicate manifest có trong helm-a)
Cụ thể về các thành phần mình sẽ giải thích ở phần sau.

# 3. Lib Chart và cách sử dụng
### Cấu trúc hệ thống

![Overview](./images/helm.drawio.png "Overview")

*Hệ thống trong bài Lab này sẽ gồm 2 namespace:*
- Normal: Triển khai một ứng dụng từ 3 Chart độc lập sử dụng ***Lib Chart, Chart***
- Address: Triển khai ứng dụng từ một Chart duy nhất sử dụng ***Lib Chart, Chart, Alias*** 

*Luồng triển khai dự án tổng quan*

1. Tại máy Client, clone source từ [helm-functional](https://gitlab.com/end2223/helm-functional "helm-functional")
2. Tại máy client tiến hành điều chỉnh một số tham số cho phù hợp, và tạo ra file template.
3. Sử dụng file template để tạo ra các tài nguyên trên Kubernetes, với (kubectl, helm hoặc ssh trực tiếp tới Master để triển khai)

### Cấu trúc thư mục quản lý dự án

|Thành phần|Mô tả|
|----------------|-------------------------|
|lib-common| Lib chart dùng chung cho tất cả các services. Đây là thành phần cốt lõi, khi có sự thay đổi có thể tác động tới toàn bộ services(Có thể kiểm soát được sự thay đổi). Được quản lý bởi người có kiến thức chuyên môn.|
|services| Chứa Chart của từng services cụ thể. Các services này có thể sử dụng cho một hoặc nhiều applications. Các thành phần này thường xuyên thay đổi. Cho phép người dùng có thể tùy chỉnh các tham số|
|aplications| Applications sẽ tập hợp các services lại để có thể tạo thành một ứng dụng cụ thể. Là thành phần ít thay đổi và đơn giản nhất.|

## 3.1 Lib chart
**Lưu ý**
- Lib Chart trong bài viết này được triển khai dưới dạng local. Bạn có thể tham khảo một số cách để quản lý Lib Chart, Chart trên một repository.

Khai báo một lib Chart. Tạo ra thư mục **lib-common**. **lib-common** gồm 3 thành phần được đặt trong thư mục "templates":
- _config.tpl
- _deployment.tpl
- _serivce.tpl

````yaml
---
# Chart.yaml
apiVersion: v2
name: lib-common
description: A Helm library chart for Kubernetes
# Type gồm 2 giá trị 
# application: chỉ rõ khi Chart này sẽ được mang đi triển khai trên K8s hoặc được tái sử dụng bởi những Chart khác. (Khi Chart được tái sử dụng bởi các Chart khác tất cả các thành phần của Chart sẽ được tự động thêm vào)
# library: được các Chart khác sủ dụng lại. Các thành phần của Lib Chart có thể thêm, bớt tuỳ ý trong Chart mới (Cần được chỉ định rõ ràng).
type: library
version: 1.0.0
appVersion: "4.0.0"
````

Định nghĩa ra các thành phần có thể tái sử dụng đặt trong thự mục "template". Các thành phần này tương ứng với các đối tượng được Kubernetes quản lý, và đã được đề cập ở trên.

Định nghĩa ra một template cho ConfigMap có tên là "lib-common.configmap". Template sẽ thực hiện đọc config có trong thư mục $configDir và tiến hành tạo ra ConfigMap.

```yaml
---
#/templates/_configmap.tpl
{{- define "lib-common.configmap" }}
# Thư mục chứa file config. Đường dẫn tới thư mục này phải ngang cấp với Chart sử dụng Lib Chart này
{{ $configDir := .Values.configMap.configDir }}
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Values.appName }}-etc
  labels:
    app: {{ .Values.appName }}
    env: {{  .Values.env }}
data:
# Load tất cả các file trong thư mục vào thành ConfigMap
{{ (.Files.Glob $configDir).AsConfig | indent 2}}
{{- end }}
```

Định nghĩa ra một template cho Deployment có tên là "lib-common.deployment"

```yaml
{{- define "lib-common.deployment" }}
{{ $configDir := .Values.configMap.configDir }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.appName }}
  labels:
    app: {{ .Values.appName }}
    env: {{  .Values.env }}
spec:
  replicas: {{ .Values.deployment.replicas }}
  selector:
    matchLabels:
      app: {{ .Values.appName }}
      env: {{  .Values.env }}
  template:
    metadata:
      annotations:
        checksum/config: {{ (.Files.Glob $configDir).AsConfig | sha256sum }}
      labels:
        app: {{ .Values.appName }}
        env: {{  .Values.env }}
    spec:
      containers:
        - name: {{ .Values.appName }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
          {{- range .Values.service.ports }}
            - name: {{ .name }}
              containerPort: {{ .targetPort }}
              protocol: {{ .protocol }}
          {{- end }}
          env:
            - name: APPNAME 
              value: {{ .Values.appName }}
          resources:
            {{- toYaml .Values.deployment.resources | nindent 12 }}
          volumeMounts:
            - name: {{ .Values.appName }}-etc
              mountPath: /app/etc/{{ .Values.appName }}.json
              subPath: {{ .Values.appName }}.json
      volumes:
        - name: {{ .Values.appName }}-etc
          configMap:
            name: {{ .Values.appName }}-etc
            defaultMode: 400
{{- end }}
```

Định nghĩa ra một template cho Service có tên là "lib-common.service"

```yaml
{{ define "lib-common.service" }}
apiVersion: v1
kind: Service
metadata:
  name: {{ .Values.appName }}
  labels:
    app: {{ .Values.appName }}
    env: {{  .Values.env }}
spec:
  type: {{ .Values.service.type }}
  ports:
    {{- if .Values.service.nodePort }}
    {{- toYaml .Values.service.ports | nindent 4 }}
    {{ else }}
    {{- range .Values.service.ports }}
    - name: {{ .name }}
      port: {{ .port }}
      targetPort: {{ .targetPort }}
      protocol: {{ .protocol }}
    {{- end }}
    {{- end }}
  selector:
    app: {{.Values.appName }}
    env: {{  .Values.env }}
{{- end}}
```
*File values.yaml, file này là một file mẫu chứa các giá trị người dùng có thể thay đổi. File này chỉ mang tính chất tham khảo khi lib chart được sử dụng. Việc sử dụng file này khi Lib chart được sử dụng bợi một Chart cụ thể*

## 3.2 Triển khai các Services(Chart) từ Lib Chart
Ở phần này chúng ta sẽ xem cách mà có thể tái sử dụng các template mà đã được định nghĩa ở **"lib-common"** trong bước trên cho một Chart cụ thể.

Với Chart ta có thể viết các file manifest cho các services theo format của Helm. Tuy nhiên để tái sử dụng, giảm chi phí nguồn lực trong việc quản lý chúng ta có thể định nghĩa các thành phần có thể tái sử dụng sau đó đóng gói vài Lib Chart như ở mục 4.1. Mỗi khi dùng đến chúng ta chỉ cần khai báo các Lib Chart và các thành phần(configMap, deployment, service) có trong Lib Chart. 

Ở thư mục **services** sẽ định nghĩa ra 3 services đơn giả. ***Các service này chỉ mang tính chất tham khảo, các services đã được build thành image và source code trong thư mục nodejs.*** Code mình viết chỉ mang tính chất tượng trưng ^.^, Mong các bạn thông cảm!
- adress
- person
- product

Trong bài viết mình sẽ lấy ví dụ cụ thể cho service *"address"*, các services còn lại có thể thực hiện tương tự.

Khai báo các lib chart mà chart triển khai dịch vụ "address" cần được sử dụng. Cụ thể ở đây là **lib-common**.

```yaml
apiVersion: v2
name: address
description: A Helm chart for Kubernetes
type: application
version: 1.0.0
appVersion: "4.0.0"

# Khai báo các lib chart, chart mà ứng dụng phụ thuộc
# Với phụ thuộc là chart, tất cả các thành phần(service, deployment...) sẽ được tự động thêm vào và không cần khai báo lại
# Với phụ thuộc là lib chart các thành phần của lib chart cần được khai báo khi muốn sử  dụng. 
dependencies:
- name: lib-common
  version: 1.0.0
  # Đường dẫn đến lib chart
  repository: "file://../../lib-common/" 

```

Vì **lib-common** là một Lib Chart, do đó ta sẽ cần khai báo những thành phần mong muốn khi sử dụng.
Cách khải báo những thành phần mà dịch vụ *"address"* cần sử dụng.

```yaml
#templates/app.yaml
# Khai báo sử dụng các thành phần đã được định nghĩa trong lib-common
# Nếu ứng dụng không sử dụng config map ta có thể loại bỏ nó
---
{{- include "lib-common.configmap" . }}
---
{{- include "lib-common.deployment" . }}
---
{{- include "lib-common.service" . }}

```
Đến đây chúng ta đã thực hiện xong phần khai báo. Để ứng dụng có thể triển khai chúng ta cần cập nhật các lib chart, chart phụ thuộc và định nghĩa file ***values.yaml***

*Thực hiện cập nhật lib chart, chart phụ thuộc*

```bash
~/helm/helm/services/address helm dep up
```
![Kết quả](./images/helm-up.png "helm dep up")

Lệnh trên sẽ thực hiện cập nhật các phụ thuộc vào thư mục chart

***Định nghĩa values.yaml***

Ở đây chúng ta chỉ cần copy lại file values.yaml trong ***lib-common*** và định nghĩa lại các tham số mong muốn.

Các bạn có thể tham khảo file values ứng với từng services. Để tránh bài viết quá dài mình sẽ chỉ đề cập những tham số cơ bản.

```yaml
# Cấu hình image và tag
image:
  repository: caongocson/app-nodejs
  tag: 18.9.0-slim
# Đường dẫn để thư mục file cấu hình tình từ thư mục hiện tại của Chart.
# Thư mục configs ngang cấp với file values trong Chart
# ~/helm/helm/services/address/configs/**
configMap:
  configDir: "configs/**"
# Loại serivce và có cho phép chế độ NodePort không?
service:
  type: NodePort
  nodePort: true
``` 

Kiểm tra thành quả

*Thực hiện cập nhật lib chart, chart phụ thuộc*
```bash
~/helm/helm/services/address helm template . > result
```

![Một phần nhỏ kết quả](./images/result.png)

Đến bước này helm sẽ tạo ra một file manifest và lưu vào trong result. Ta có thể mang file manifest đi triển khai trên server hoặc triển khai trực tiếp với helm.

Sẽ có nhiều kỹ thuật triển khai, trong bài viết này mình sẽ không đề cập đến vấn đề này.

Thực hiện tương tự ta sẽ triển khai được cho hai dịch vụ còn lại:
- person
- product
# 4. Triển khai Applications từ các Services
Triển khai một ứng dụng cụ thể .

## 4.1 Ứng dụng triển khai thông thường
Giả sử ứng dụng triển khai cần các dịch vụ sau:
- address
- person
- product

Tạo ra thư mục **"applications/normal"**

```yaml
# Chart.yaml
apiVersion: v2
name: normal
description: A Helm chart for Kubernetes
type: application
version: 1.0.0
appVersion: "4.0.0"
dependencies:
- name: address
  version: 1.0.0
  repository: "file://../../services/address/" 
- name: person
  version: 1.0.0
  repository: "file://../../services/person/" 
- name: product
  version: 1.0.0
  repository: "file://../../services/product/" 
```

Do ở đây các phụ thuộc đều là các Chart nên chúng ta sẽ không cần khai báo lại các thành phần có trong Chart. Tất cả các thành phần có trong 3 Chart trên sẽ được thêm vào trong Chart có tên là **normal**.

*Cập nhật các Chart phụ thuộc*

```bash
~/helm/helm/applications/normal helm dep up
``` 

*Ghi đè lại file values cho từng chart*

Với tên của Chart muốn ghi đè đi đầu và theo sau là các giá trị muốn ghi đè:

```yaml
address:
  appName: address-normal
  image:
    pullPolicy: IfNotPresent
  service:
    type: CLusterIP
    nodePort: false

person:
  appName: person-normal

product:
  appName: product-normal
  image:
    pullPolicy: IfNotPresent
```

Kiểm tra thành quả

```bash
~/helm/helm/applications/normal helm template . > result
```

![Normla](./images/normal.png)

Ở bước này chúng ta cần kiểm tra các thông số để xác nhận các tham số đã được cập nhật chính xác.

Có thể thấy với việc triển khai các manifest dưới dạng lib-chart, chart, đến khi triển khai chúng ta chỉ cần khai báo services cần dùng và ghi lại một số tham số cần thiết là có thể triển khai được ứng dụng

## 4.2 Alias
***Lưu ý***
- Ở phần này chỉ mang tính chất giới thiệu. Viện triển khai chắc chắn sẽ có lỗi. Nếu các bạn muốn kiểm tra bạn nên sửa cấu hình image, repository, và cần chuẩn bị config cho các service trước khi chạy =)).

Đây là phần mình muốn nhấn mạnh nhất trong toàn bộ bài viết. Với "Alias" mình đã không cần phải duplicate một service thành 2 hoặc nhiều Chart tương tự nhau để có thể dùng lại.

Ví dụ bạn có dịch vụ "address" và bạn đã viết cho nó một chart có tên là "address". Và một ứng dụng "address" cần triển khai 2 dịch vụ "address" **đọc dữ liệu từ hai cơ sở dữ liệu khác nhau.**

Với các charts được khai báo trong file Chart.yaml. *Bạn chỉ có thể khai báo được một chart duy nhất với tên là ***address***. Vậy làm thế nào để bạn triển khai được 2 dịch vụ kết nối đến hai cơ sở dữ liệu tách biệt nhau mà không phải tạo thêm một Chart với tên là ***address_2***?

***alias*** sẽ giúp bản giải quyế t vấn đề này. Giả sử trong ứng dụng "address" chúng ta triển khai hai dịch vụ:
- addressHN: Đọc cơ sở dữ liệu từ site Hà Nội.
- addressSG: Đọc cơ sở dữ liệu từ site Sài Gòn.

Chúng ta sẽ thể hiện trong file Chart.yaml như sau:

```yaml
#Chart.yaml
apiVersion: v2
name: alias
description: A Helm chart for Kubernetes
type: application
version: 1.0.0
appVersion: "4.0.0"
dependencies:
- name: address
  version: 1.0.0
  repository: "file://../../services/address/" 
  alias: addressHN
- name: address
  version: 1.0.0
  repository: "file://../../services/address/" 
  alias: addressSG
```

Như vậy từ một chart chúng ta có thể tạo ra 2 dịch vụ với cấu hình khác nhau trong cùng một ứng dụng. Việc cần làm bây giờ là ghi đè lại một số tham số cần thiết, trong đó quan trọng nhất là tham số để chỉ rõ đường dẫn đọc file cấu hình của từng site:

```yaml
#values.yaml
addressHN:
  appName: address-HN
  image:
    # Giá trị mang tính chất tượng trưng
    repository: docker-registry.hn.com/app-nodejs
    pullPolicy: IfNotPresent
#    Cấu hình config cho từng site
  configMap:
    configDir: "configs/hn/**"
addressSG:
  appName: address-SG
  image:
    repository: docker-registry.sg.com/app-nodejs
    pullPolicy: IfNotPresent
#    Cấu hình config cho từng site
  configMap:
    configDir: "configs/sg/**"
  
```
Tiến hành update chart và tạo ra file template tương tự như các bước trên. Chúng ta sẽ nhận được kết quả mong muốn, chỉ với một chart chúng ta có thể triển khai đồng thời 2 service với các cấu hình khác nhau cùng một lúc.

# 5. Kết luận
- Cảm ơn các bạn đã dành thời gian đọc bài ^.^.
- Đây cũng là lần đầu tiên mình viết một bài viết chia sẻ đến cộng đồng, sẽ không tránh khỏi những sai xót. Vậy rất mong các bạn thông cảm và đưa ra những lời góp ý!!!
