{{- define "lib-common.deployment" }}
{{ $configDir := .Values.configMap.configDir }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.appName }}
  labels:
    app: {{ .Values.appName }}
    env: {{  .Values.env }}
spec:
  replicas: {{ .Values.deployment.replicas }}
  selector:
    matchLabels:
      app: {{ .Values.appName }}
      env: {{  .Values.env }}
  template:
    metadata:
      annotations:
        checksum/config: {{ (.Files.Glob $configDir).AsConfig | sha256sum }}
      labels:
        app: {{ .Values.appName }}
        env: {{  .Values.env }}
    spec:
      containers:
        - name: {{ .Values.appName }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
          {{- range .Values.service.ports }}
            - name: {{ .name }}
              containerPort: {{ .targetPort }}
              protocol: {{ .protocol }}
          {{- end }}
          env:
            - name: APPNAME 
              value: {{ .Values.appName }}
          resources:
            {{- toYaml .Values.deployment.resources | nindent 12 }}
          volumeMounts:
            - name: {{ .Values.appName }}-etc
              mountPath: /app/etc/{{ .Values.appName }}.json
              subPath: {{ .Values.appName }}.json
      volumes:
        - name: {{ .Values.appName }}-etc
          configMap:
            name: {{ .Values.appName }}-etc
            defaultMode: 400
{{- end }}