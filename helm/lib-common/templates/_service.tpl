{{ define "lib-common.service" }}
apiVersion: v1
kind: Service
metadata:
  name: {{ .Values.appName }}
  labels:
    app: {{ .Values.appName }}
    env: {{  .Values.env }}
spec:
  type: {{ .Values.service.type }}
  ports:
    {{- if .Values.service.nodePort }}
    {{- toYaml .Values.service.ports | nindent 4 }}
    {{ else }}
    {{- range .Values.service.ports }}
    - name: {{ .name }}
      port: {{ .port }}
      targetPort: {{ .targetPort }}
      protocol: {{ .protocol }}
    {{- end }}
    {{- end }}
  selector:
    app: {{.Values.appName }}
    env: {{  .Values.env }}
{{- end}}