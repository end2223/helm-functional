{{- define "lib-common.configmap" }}
{{ $configDir := .Values.configMap.configDir }}
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Values.appName }}-etc
  labels:
    app: {{ .Values.appName }}
    env: {{  .Values.env }}
data:
{{ (.Files.Glob $configDir).AsConfig | indent 2}}
{{- end }}