const fs = require('fs');
var data;

fs.readdir("etc/", (err, filenames) => {
    if (err) throw err;
    filenames.forEach(function (filename) {
        data = fs.readFileSync("etc/" + filename, {encoding:'utf8', flag:'r'});
        data = JSON.parse(data);
    });
})
var express = require('express');
var app = express();
let port = process.env.PORT || 3000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/address', (req, res, next) => {
    res.json(data);
});
app.get('/address/:id', (req, res, next) => {
    if(req.params.id>0 && req.params.id<= data.length){
        let tmp = data.filter(i => i.id==(req.params.id));
        res.json(tmp);
    }
    else {
        res.json("Object is not exist!");
    }
    
});

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});